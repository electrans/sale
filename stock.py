# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.pyson import Eval, Bool, Id
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['StockMove', 'Package', 'ShipmentOut']


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    sales = fields.Function(fields.One2Many('sale.sale', None, 'Sales'),
                            'get_sales')
    filename = fields.Function(fields.Char('Filename'), 'get_filename')

    # Used for stock.shipment.out email template
    project_manager_mails = fields.Function(
        fields.Char('Project Manager Emails'), 'get_project_manager_mails')

    @classmethod
    def __setup__(cls):
        super(ShipmentOut, cls).__setup__()
        cls._buttons.update({
                'create_shipping': {
                    'invisible': True,
                    },
                })

    def get_sales(self, name):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        move_ids = [x.id for x in self.outgoing_moves]
        with Transaction().set_context(_check_access=False):
            sale_lines = SaleLine.search([
                    ('moves', 'in', move_ids),
                    ])
            return list(set(l.sale.id for l in sale_lines))

    def get_filename(self, name):
        filename = []
        if self.sales:
            filename.append(self.sales[0].party.supplier_code)
            description = self.sales[0].description.replace('/',
                                                            '_').replace(
                ' ', '')
            filename.append(description)
        filename.append(self.number)
        return '-'.join(filename) + '.zip'

    def get_project_manager_mails(self, name=None):
        # Get project manager emails
        # Use set() to not get duplicated elements
        emails = set(sale.project.project_manager.party.email
                     for sale in self.sales if sale.project and
                     sale.project.project_manager and
                     sale.project.project_manager.party and
                     sale.project.project_manager.party.email)
        return ', '.join(email for email in emails)

    def _sync_outgoing_move(self, template=None):
        move = super(ShipmentOut, self)._sync_outgoing_move(template)
        # template is the outgoing move which the new move is created from
        # If the template move doesn't have package_type, the origin sale_line and its sale won't have either
        # So we directly drag the package_type's value that had the template move
        move.package_type = template.package_type if template else None
        return move


class StockMove(metaclass=PoolMeta):
    __name__ = 'stock.move'

    sale_line = fields.Function(fields.Many2One('sale.line', 'Sale Line'),
        'get_sale_line')
    package_type = fields.Many2One(
        'stock.package.type', "Package Type",
        states={
            'readonly': Eval('state').in_(['cancelled', 'packed', 'done']),
            'invisible': ~Eval('sale_line'),
            },
        depends=['state', 'sale_line'])

    def get_sale_line(self, name):
        # Used for export reports, to get fields from the sale_line of the move
        pool = Pool()
        Line = pool.get('sale.line')
        return self.origin.id if isinstance(self.origin, Line) else None


class Package(metaclass=PoolMeta):
    __name__ = 'stock.package'

    package_volume = fields.Float('Volume',
                          digits=(16, Eval('package_volume_digits', 2)),
                          states={
                              'readonly': ~Bool(Eval('width') & Eval('length') & Eval('height')),
                          },
                          depends=['package_volume_digits', 'width', 'length', 'height'],
                          help="Will be computed automatically when the measurements are filled and in the same uom."
                                  )
    package_volume_uom = fields.Many2One('product.uom', 'Volume Uom',
                                 domain=[('category', '=', Id('product', 'uom_cat_volume'))],
                                 states={
                                     'required': Bool(Eval('volume')),
                                     'readonly': ~Bool(Eval('width') & Eval('length') & Eval('height')),
                                 },
                                 depends=['volume', 'width', 'length', 'height'])
    package_volume_digits = fields.Function(fields.Integer('Volume Digits'),
                                            'on_change_with_package_volume_digits')

    @fields.depends('width', 'length', 'height', 'length_uom', 'height_uom', 'width_uom')
    def on_change_with_package_volume(self, name=None):
        package_volume = 0
        if (self.width and self.length and self.height) and (
                self.length_uom == self.height_uom == self.width_uom and self.length_uom.rate):
            # Use length_uom cause all the uom are the same
            if (self.length_uom.factor and self.length_uom.rate) == 1:
                package_volume = round(self.width * self.length * self.height, self.package_volume_digits)
            else:
                package_volume = round(
                    (self.width * self.length * self.height) / self.length_uom.rate, self.package_volume_digits)
        return package_volume

    @fields.depends('type')
    def on_change_type(self):
        super().on_change_type()
        if self.type:
            self.package_volume = self.on_change_with_package_volume()

    @fields.depends('package_volume_uom')
    def on_change_with_package_volume_digits(self, name=None):
        return (self.package_volume_uom.digits if self.package_volume_uom
                else self.default_package_volume_digits())

    @staticmethod
    def default_package_volume_uom():
        pool = Pool()
        Uom = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')
        cubic_meter = Uom(ModelData.get_id('product', 'uom_cubic_meter'))
        return cubic_meter.id

    @staticmethod
    def default_package_volume_digits():
        return 2

    @classmethod
    def __setup__(cls):
        super(Package, cls).__setup__()
        cls.moves.domain += [('package_type', 'in', [Eval('type'), None]),
                             ('quantity', '>', 0)
                             ]
        cls.moves.depends.append('type')
