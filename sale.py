# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelView, Workflow, ModelSQL
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Id, If, Not
from trytond import backend
from trytond.transaction import Transaction
from trytond.wizard import Wizard, Button, StateView, StateTransition, StateAction
from trytond.tools import grouped_slice, reduce_ids
from trytond.modules.company.model import CompanyValueMixin
import re
from docutils.core import publish_parts
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning
from trytond.modules.electrans_tools.tools import generate_line_positions, move_lines
import logging
from decimal import Decimal
from trytond.modules.sale.sale import get_shipments_returns, search_shipments_returns

try:
    from jinja2 import Template as Jinja2Template

    jinja2_loaded = True
except ImportError:
    jinja2_loaded = False
    logging.getLogger('electrans_sale').error(
        'Unable to import jinja2. Install jinja2 package.')

__all__ = ['Sale', 'SaleLine', 'SaleGenerateLinePositions']


class SaleCategory(ModelView, ModelSQL):
    'Sale Category'
    __name__ = 'sale.category'
    _rec_name = 'name'

    name = fields.Char('Name')


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    tax = fields.Function(
        fields.Many2One('account.tax', "Tax"),
        'get_tax')
    particular_terms = fields.Text(
        "Particular terms",
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Bool(Eval('state') == 'draft') & ~Bool(Eval('preview_terms'))
        },
        depends=['state', 'preview_terms'])
    general_terms = fields.Many2One(
        'typified.description', "General terms",
        states={
            'readonly': Eval('state') != 'draft'
        },
        depends=['state'])
    # TODO: [APP-927] Analize why this field is a multivalue field in party model and if it has to be here too.
    lang = fields.Many2One(
        'ir.lang', "Language",
        help='Language for the report',
        states={
            'readonly': Bool(Eval('particular_terms')) | Bool(Eval('state') != 'draft')
        },
        depends=['particular_terms', 'state'])
    preview_terms = fields.Function(fields.Boolean("Preview Terms",
                                                   states={
                                                       'invisible': Bool(Eval('state') != 'draft')
                                                   },
                                                   depends=['state']),
                                    'get_preview_terms',
                                    setter='set_preview_terms')
    particular_terms_html = fields.Function(fields.Text("Particular terms HTML"),
                                            'get_particular_terms_html')
    general_terms_html = fields.Function(fields.Text("General terms HTML"),
                                         'get_general_terms_html')
    address = fields.Many2One(
        'party.address', "Address",
        domain=[('party', '=', Eval('party'))],
        states={
            'readonly': Eval('state') != 'draft',
            'required': ~Eval('state').in_(['draft', 'quotation', 'cancelled'])},
        depends=['state', 'party'])
    # It is used in the sale report to know if there are lines with discount
    lines_with_discount = fields.Function(
        fields.Boolean("Lines with discont"),
        'get_lines_with_discount')
    category = fields.Many2One('sale.category', 'Category',
                               states={
                                   'readonly': Eval('state') != 'draft',
                                   'invisible': Eval('not_from_presupuestario', False) == False},
                               depends=['state', 'not_from_presupuestario', 'is_order'])
    country = fields.Many2One('country.country', 'Country',
                              states={
                                  'readonly': Eval('state') != 'draft',
                                  'invisible': Eval('not_from_presupuestario', False) == False},
                              depends=['state', 'not_from_presupuestario', 'is_order'])
    # Used in sales email template
    shipment_numbers = fields.Function(fields.Char('Shipment Numbers'),
                                       'get_shipment_numbers')
    package_type = fields.Many2One(
        'stock.package.type', "Package Type",
        states={
            'readonly': Eval('state').in_(['cancelled', 'processing', 'done']),
            },
        depends=['state'])
    internal_shipments = fields.Function(fields.Many2Many(
            'stock.shipment.internal', None, None, "Internal Shipments",
            states={
                'invisible': ~Eval('internal_shipments'),
            }),
        'get_internal_shipments', searcher='search_internal_shipments')

    get_internal_shipments = get_shipments_returns('stock.shipment.internal')
    search_internal_shipments = search_shipments_returns('stock.shipment.internal')

    @classmethod
    def __register__(cls, module_name):
        table = backend.TableHandler(cls, module_name)
        if table.column_exist('customer_reference'):
            with Transaction().connection.cursor() as cursor:
                query = "update sale_sale set customer_reference=reference where reference>'' " + \
                        "and customer_reference<>reference and substring(reference,customer_reference) is not null;"
                cursor.execute(query)
                query = "update sale_sale set description=reference where reference>'' " + \
                        "and description=customer_reference;"
                cursor.execute(query)
                query = "update sale_sale set description=description||'. '||reference where reference>'' " + \
                        "and substring(customer_reference,reference) is null " + \
                        "and substring(description,reference) is null and description<>reference;"
                cursor.execute(query)

            table.column_rename('reference', 'reference_old')
            table.column_rename('customer_reference', 'reference')
        super(Sale, cls).__register__(module_name)

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                super(Sale, cls).search_rec_name(name, clause),
                [('description',) + tuple(clause[1:])],
                [('full_number',) + tuple(clause[1:])]
                ]

    def get_tax(self, name):
        for line in self.lines:
            if line.taxes:
                return line.taxes[0].id

    @fields.depends('terms', 'lang')
    def on_change_with_particular_terms(self, name=None):
        def get_user():
            User = Pool().get('res.user')
            user = None
            if Transaction().user:
                user = User(Transaction().user)
            return {
                'user': user,
            }

        def get_field_translation(lang, field, id):
            Translation = Pool().get('ir.translation')
            translation = Translation.search([('name', '=', field), ('lang', '=', lang), ('res_id', '=', id)], limit=1)
            return translation[0].value if translation else ''

        text = '\n'.join(
            ['- ' + get_field_translation(self.lang.code, t.__name__ + ',description', t.id)
             for t in self.terms])

        template = Jinja2Template(text)
        record = {'record': self, 'user': get_user()}
        return template.render(record)

    @fields.depends('address')
    def on_change_party(self):
        super(Sale, self).on_change_party()
        if self.party:
            if self.party.lang:
                self.lang = self.party.lang.id
        if self.invoice_address and not self.invoice_address.invoice:
            self.invoice_address = None
        self.address = None
        if not self.address and self.party:
            self.address = self.party.address_get(type='invoice')
        self.shipment_address = None

    def get_preview_terms(self, _):
        return self.state != 'draft' or self.particular_terms

    def set_preview_terms(self, name, _):
        'Dummy function to enable the check'
        pass

    def get_particular_terms_html(self, _):
        return publish_parts(self.particular_terms, writer_name='html')['body'] if self.particular_terms else ''

    def get_general_terms_html(self, _):
        if self.general_terms:
            return publish_parts(self.general_terms.description, writer_name='html')['body']

    def _get_invoice_sale(self):
        # Do not copy the sale invoice_contact to the invoice invoice_contact
        invoice = super(Sale, self)._get_invoice_sale()
        if self.invoice_contact:
            invoice.invoice_contact = None
        return invoice

    def create_invoice(self):
        invoice = super(Sale, self).create_invoice()
        if invoice:
            invoice.sale_reference = self.reference
            invoice.save()
        return invoice

    @staticmethod
    def check_domains(sales):
        SaleLine = Pool().get('sale.line')
        sale_ids = [sale for sale in sales if sale.state in [
            'draft', 'quotation'] and not isinstance(sale.origin, Sale)]
        # Check if there are products not salables
        not_salables = SaleLine.search([
            ('sale', 'in', sale_ids),
            ('product.salable', '=', False)])
        if not_salables:
            not_salables = ', '.join([p.rec_name for p in not_salables])
            raise UserError(gettext(
                'electrans_sale.not_salable_products',
                product=not_salables))

    @classmethod
    def quote(cls, sales):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        Sale.check_domains(sales)
        for sale in sales:
            if not sale.is_order and sale.not_from_presupuestario and (
                    not sale.country or not sale.category):
                raise UserError(
                    gettext('electrans_sale.required_country_and_category'))
            if sale.price_list:
                pl_product_ids = [line.product.id
                                  for line in sale.price_list.lines
                                  if line.product]
                for product in cls.products_to_check(sale):
                    if product.id not in pl_product_ids:
                        key = 'product_price_list_%s_%s' % (
                            product.id, sale.id)
                        if Warning.check(key):
                            raise UserWarning(key, gettext(
                                'electrans_sale.not_in_price_list',
                                product=product.rec_name))
        cls.store_cache(sales)
        # sale quotation code
        Config = pool.get('sale.configuration')
        config = Config(1)
        Date = pool.get('ir.date')
        for sale in sales:
            if not sale.is_order:
                sale.quotation_date = Date.today()
                if not sale.in_force_period:
                    sale.in_force_period = config.default_in_force_period
        cls.save(sales)
        super(Sale, cls).quote(sales)

    @staticmethod
    def products_to_check(sale):
        return [line.product for line in sale.lines if line.product]

    def get_lines_with_discount(self, name=None):
        for line in self.lines:
            if line.discount:
                return True

    def get_shipment_numbers(self, name=None):
        # Get all shipment numbers from a sale
        return ', '.join(shipment.number for shipment in self.shipments)

    # sale quotation code
    _history = True

    quotation_date = fields.Date(
        "Quotation Date",
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Bool(Eval('is_order'))
        },
        depends=['state', 'is_order'])
    in_force_period = fields.TimeDelta(
        "Force Period",
        states={
            'required': Bool(Eval('state') != 'draft') & Not(Bool(Eval('is_order'))),
            'readonly': Eval('state') != 'draft',
            'invisible': Bool(Eval('is_order'))
        },
        depends=['state', 'is_order'])
    in_force = fields.Function(
        fields.Boolean("In force"),
        'get_in_force')
    orders = fields.One2Many(
        'sale.sale', 'origin', "Orders",
        states={
            'readonly': True,
            'invisible': Bool(Eval('is_order'))})
    quotation = fields.Function(
        fields.Many2One('sale.sale', "Quotation",
                        states={
                            'invisible': (~Bool(Eval('is_order')) |
                                          (~Bool(Eval('not_from_presupuestario'))))}),
        'get_quotation')
    is_order = fields.Function(
        fields.Boolean("Is Order"),
        'get_is_order',
        searcher='search_is_order')
    revision = fields.Integer(
        "revision", required=False, readonly=True,
        states={
            'invisible': Eval('revision', 0) == 0,
        })
    order_idx = fields.Integer(
        "Order number", required=False, readonly=True,
        states={
            'invisible': Eval('order_idx', 0) == 0,
        })
    full_number = fields.Function(
        fields.Char("Number"),
        'get_full_number',
        searcher='search_full_number')
    not_from_presupuestario = fields.Boolean(
        "Not from Presupuestario")
    presupuestario_number = fields.Char(
        "Presupuestario Number",
        states={
            'invisible': Bool(Eval('not_from_presupuestario')),
            'readonly': Eval('state') != 'draft'
        },
        depends=['not_from_presupuestario', 'state'])
    quotation_number = fields.Function(
        fields.Char("Quotation"),
        'get_quotation_number')
    quotation_state = fields.Function(fields.Selection([
        (None, ''),
        ('in_progress', 'In Progress'),
        ('won', 'Won'),
        ('lost', 'Lost'),
    ], "Quotation State",
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Bool(Eval('is_order'))
        },
        depends=['state', 'is_order']), 'get_quotation_state')

    def get_quotation(self, _):
        if not self.is_order:
            return None
        if not self.not_from_presupuestario:
            return None  # self.presupuestario_number
        return self.origin.id if self.origin else None

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.invoice_address.domain += [
            If(Eval('state') != 'draft',
               [('invoice', '=', True)], [])]
        cls.carrier.states['readonly'] = ~Eval('state').in_(['draft', 'quotation'])
        if not cls.shipment_party.help:
            cls.shipment_party.help = "Party where to send the products. Fill only if it is different from customer."
        for field in ('reference', 'origin', 'sale_date', 'invoice_method', 'shipment_method', 'shipment_cost_method'):
            getattr(cls, field).states['invisible'] = ~Bool(Eval('is_order'))
            getattr(cls, field).depends += ['is_order']
        cls._transitions.add(('draft', 'confirmed'))
        cls._buttons['confirm']['depends'] += ['is_order']
        # do no show confirm button if it's a quotation
        cls._buttons['confirm']['invisible'] = If(Bool(Eval('is_order')),
                                                  ~Eval('state').in_(['draft', 'quotation']),
                                                  True)
        cls._buttons['confirm']['depends'] += ['is_order']
        # do not show quote button if it's an order
        cls._buttons['quote']['invisible'] |= Bool(Eval('is_order'))
        cls._buttons['quote']['depends'] += ['is_order']
        # set button draf without increment revision number
        cls._buttons.update({
            'set_draft_without_revision': {
                'invisible': ~Eval('state').in_(['quotation', 'confirmed']) |
                     (Not(Bool(Eval('not_from_presupuestario'))) |
                      Bool(Eval('is_order'))),
                'readonly': Bool(Eval('orders', False)),
                'depends': ['state', 'is_order'],
                'icon': If(Eval('state') == 'cancelled',
                           'tryton-undo',
                           'tryton-back'),
            },
        })
        # set all fields to readonly when the sale have orders assigned
        for fname in dir(cls):
            if fname != 'orders':
                field = getattr(cls, fname)
                if hasattr(field, 'states'):
                    if not isinstance(field, fields.Function) or isinstance(field, fields.MultiValue):
                        field.depends.append('orders')
                        if 'readonly' in field.states:
                            field.states['readonly'] |= Bool(Eval('orders', False))
                        else:
                            field.states['readonly'] = Bool(Eval('orders', False))
        # set all buttons to readonly when the sale have orders assigned
        for button, states in cls._buttons.items():
            if 'readonly' in states:
                states['readonly'] |= Bool(Eval('orders', False))
            else:
                states['readonly'] = Bool(Eval('orders', False))
        cls.party.domain.append(
            If(Eval('is_order'),
               ('allowed_for_sales', '=', True),
               ()))
        cls.party.depends.append('is_order')
        cls._states_cached.append('quotation')

    @classmethod
    def copy(cls, sales, default=None):
        def get_next_order_idx(sale_number, sale_revision):
            last_order_idx = 0
            sales = cls.search([
                ('number', '=', sale_number),
                ('revision', '=', sale_revision),
                ('is_order', '=', True)],
                order=[('order_idx', 'DESC')], limit=1) if sale_number else None
            if sales and sales[-1].order_idx:
                last_order_idx = sales[-1].order_idx
            return last_order_idx + 1

        Sale.check_domains(sales)
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['orders'] = None
        default['shipments'] = None
        default['invoices'] = None
        default['employee'] = cls.default_employee()

        result = []
        for sale in sales:
            is_order = sale.is_order and sale.origin and \
                       isinstance(sale.origin, Sale)
            if default.get('new_order_from_wizard'):
                is_order = True
            if is_order:
                default['number'] = sale.number
                default['revision'] = sale.revision
                default['order_idx'] = get_next_order_idx(sale.number, sale.revision)
                default.setdefault('reference', None),
                default['sale_date'] = None
            else:
                default['number'] = None
                default['revision'] = None
                default['order_idx'] = None
                default['sale_date'] = None
                default['quotation_date'] = None
            result += super(Sale, cls).copy([sale], default=default)
        return result

    @classmethod
    def delete(cls, sales):
        for sale in sales:
            if sale.orders:
                raise UserError(gettext('electrans_sale.has_orders'))
        super(Sale, cls).delete(sales)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, sales):
        # Check context
        if not Transaction().context.get('skip_increment_revision'):
            cursor = Transaction().connection.cursor()
            table = cls.__table__()
            sales = [sale for sale in sales if not sale.is_order]
            # Use SQL and before super to avoid two history entries
            for sub_sales in grouped_slice(sales):
                rev = table.revision if table.revision else 0
                cursor.execute(*table.update(
                    [rev],
                    [rev + 1],
                    where=reduce_ids(table.id, sub_sales)))

        super().draft(sales)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, sales):
        for sale in sales:
            if sale.is_order:
                cls.quote(sales)
            sale.check_for_quotation()
        super(Sale, cls).confirm(sales)

    def check_for_quotation(self):
        if self.state == 'draft':
            pass
        else:
            super(Sale, self).check_for_quotation()

    @fields.depends('in_force_period', 'quotation_date')
    def get_in_force(self, name):
        Date = Pool().get('ir.date')
        if self.in_force_period and self.quotation_date and self.quotation_date + self.in_force_period >= Date.today():
            return True

    def get_is_order(self, name=None):
        if self.order_idx or not self.not_from_presupuestario:
            return True

    @classmethod
    def search_is_order(cls, name, clause):
        pool = Pool()
        Sale = pool.get('sale.sale')
        condition = 'in' if clause[2] else 'not in'
        orders = Sale.search(['OR', ('order_idx', '!=', None), ('not_from_presupuestario', '=', False)])
        p_ids = [o.id for o in orders]
        return [('id', condition, p_ids)]

    @classmethod
    def set_number(cls, sales):
        # Set the sale number with a different sequence depending on where them are created
        pool = Pool()
        Config = pool.get('sale.configuration')

        config = Config(1)
        if not config.sale_quotation_sequence:
            # Show raise error if sale quotation default sequence is not set
            raise UserError(gettext('electrans_sale.no_quotation_sequence'))

        for sale in sales:
            if sale.number:
                continue
            # get the number of the two sequences to keep them synchronized
            if sale.not_from_presupuestario:
                sale.number = config.sale_quotation_sequence.get()
                sale.revision = 0
        super().set_number(sales)

    @fields.depends('number', 'revision', 'order_idx', 'is_order')
    def get_full_number(self, name):
        if not self.number:
            return ''
        full_number = self.number
        if self.revision:
            full_number += 'R' + str(self.revision)
        if self.is_order and self.order_idx:
            full_number += '/' + str(self.order_idx)
        return full_number

    @classmethod
    def search_full_number(cls, name, clause):
        res = []
        # split by the '/' char because is the separator between the 'order_idx' field and the rest
        _split = clause[2].replace('%', '').split('/')
        if clause[1] == 'ilike':
            if len(_split) == 2:
                res.append(('order_idx', '=', int(_split[1])))
            # split by the 'R' char because is the separator between the revision and the number fields
            # do not use the first 3 characters because the number prefix, can contain an 'R'
            _split2 = _split[0][3:].split('R')
            if len(_split2) == 2:
                res.append(('revision', '=', int(_split2[1])))
            # concat again the prefix with the number
            res.append(('number', '=', _split[0][:3] + _split2[0]))
        return res

    def get_quotation_number(self, name):
        quotation_number = ''
        if self.origin and isinstance(self.origin, Sale):
            quotation_number = self.origin.full_number
        elif self.presupuestario_number:
            quotation_number = self.presupuestario_number
        return quotation_number

    def get_quotation_state(self, name):
        """
        Return the state of quotation sale.
        - in_progress When the sale is in draft or quotation state
        - won When exists a sale order 'not_from_presupuestario', '=', True and is_order', '=', True Related sale
        - lost when quotation state is cancelled
        """
        if self.orders:
            return 'won'
        if self.state == 'cancelled':
            return 'lost'
        return 'in_progress' if self.number else None

    @classmethod
    def view_attributes(cls):
        """
            Color the quotation_state field in green if won
        """
        attributes = super().view_attributes() + [
            ('/tree//field[@name="quotation_state"]', 'visual',
             If(
                 Eval('quotation_state') == 'lost',
                 'danger',
                 If(Eval('quotation_state') == 'won',
                    'success',
                    'muted')))
        ]
        # Hide lead time buttons in modify header wizard
        if Transaction().context.get('modify_header'):
            attributes.extend([
                ('//group[@id="buttons_line2"]', 'states',
                 {'invisible': True}),
            ])
        return attributes

    @classmethod
    @ModelView.button
    def set_draft_without_revision(cls, sales):
        # Set sale to draft without increment the revision
        with Transaction().set_context(skip_increment_revision=True):
            cls.draft(sales)

    def compute_shipment_cost(self):
        """
           Override to not add the cost of the shipment into sale line if price is 0
        """
        currency = super(Sale, self).compute_shipment_cost()
        if currency and currency == 0:
            return None

    @property
    def previous_invoiced_valued_relation(self):
        # Used for invoice valued relation report, to get totals of the already invoiced amounts
        previous_invoiced_dict = {
            'amounts_summary': 0,
            'general_expense': 0,
            'industrial_benefit': 0
        }
        for invoice in self.invoices:
            if invoice.state in ['posted', 'paid']:
                previous_invoiced_dict['amounts_summary'] += invoice.gross_amount
                previous_invoiced_dict['general_expense'] += invoice.general_expense_amount
                previous_invoiced_dict['industrial_benefit'] += invoice.industrial_benefit_amount
        return previous_invoiced_dict


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    _history = True

    position = fields.Char(
        "Position",
        states={
            'invisible': Eval('type').in_(['total', 'subtotal', 'subsubtotal'])
        },
        depends=['type'])
    sale_party = fields.Function(
        fields.Many2One('party.party', 'Sale Party'),
        'get_sale_party',
        searcher='search_sale_party')
    sale_date = fields.Function(
        fields.Date('Sale Date'),
        'get_sale_date',
        searcher='search_sale_date')
    shipment_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('waiting', 'Waiting'),
            ('partially_received', 'Partially Received'),
            ('received', 'Received')],
            "Shipment State"),
        'get_shipment_state')
    invoice_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('waiting', 'Waiting'),
            ('exception', 'Exception'),
            ('posted', 'Posted'),
            ('paid', 'Paid')],
            "Invoice State"),
        'get_invoice_state')
    price_list_line_position = fields.Function(
        fields.Char('Price List Position'),
        'on_change_with_price_list_line_position')
    quotation_line = fields.Many2One('sale.line', 'Quotation Line', readonly=True)
    package_type = fields.Many2One(
        'stock.package.type', "Package Type",
        states={
            'readonly': Eval('sale_state').in_(['cancelled', 'processing', 'done']),
            'invisible': Eval('type') != 'line',
            },
        depends=['sale_state', 'type'])
    invoiced_amount = fields.Function(fields.Numeric(
        'Invoiced Amount',
        digits=(16, Eval('_parent_sale', {}).get('currency_digits', 2))),
        'get_invoiced_amount')

    @classmethod
    def get_invoiced_amount(cls, sale_lines, names):
        invoiced_amount = {}
        for line in sale_lines:
            invoiced_amount[line.id] = sum(
                (inv_line.amount for inv_line in line.invoice_lines
                 if inv_line.invoice and inv_line.invoice.state in ['posted', 'paid']), Decimal(0))
        return {'invoiced_amount': invoiced_amount}

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        cls._buttons.update({
            'set_description_lowercase': {
                'readonly': Eval('sale_state') != 'draft',
                'depends': ['sale_state']}})
        cls.shipping_date.states['invisible'] = False
        cls.manual_delivery_date.states['readonly'] = Bool(
            Eval('_parent_sale', {}).get('orders'))

    @classmethod
    def create(cls, vlist):
        for values in vlist:
            if 'type' in values and values['type'] != 'line':
                values['taxes'] = []
        return super(SaleLine, cls).create(vlist)

    def get_rec_name(self, name):
        name = super(SaleLine, self).get_rec_name(name)
        if self.position:
            name += ' Pos.: ' + str(self.position)
        return name

    @classmethod
    @ModelView.button
    def set_description_lowercase(cls, lines):
        to_save = []
        for line in lines:
            description = []
            for sentence in re.split('([.!?\\]\n] *)', line.description):
                words = sentence.split(' ')
                first_word = True
                for word in words:
                    # search if the word contains any number
                    numbers = re.search("\\d", word)
                    if numbers:
                        description.append(word + ' ')
                    elif first_word:
                        description.append(word.capitalize() + ' ')
                    elif len(word) == 1:
                        description.append(word)
                    else:
                        description.append(word.lower() + ' ')
                    first_word = False
            line.description = ''.join([word for word in description])
            to_save.append(line)
        cls.save(to_save)

    @fields.depends('product', 'quantity', 'gross_unit_price')
    def on_change_quantity(self):
        if not self.gross_unit_price:
            super(SaleLine, self).on_change_quantity()

    def get_purchase_request(self):
        Request = Pool().get('purchase.request')
        request = super(SaleLine, self).get_purchase_request()
        if request:
            request.save()
            # If there are product_suppliers related to the product but the purchase request have no customer,
            # raise an error to notify the user to change the delivery_date line
            if not request.customer and self.product.product_suppliers:
                raise UserError(gettext('electrans_sale.supplier_delivery_date', product=self.product.rec_name))
            Request.to_pending([request])
        return request

    @staticmethod
    def default_taxes():
        # get the default tax from sale configuration
        SaleConfiguration = Pool().get('sale.configuration')
        taxes = []
        default_tax = SaleConfiguration(1).default_tax
        if default_tax:
            taxes = [default_tax.id]
        return taxes

    @fields.depends('product', 'taxes', 'sale', '_parent_sale.party', 'type')
    def on_change_taxes(self):
        super(SaleLine, self).on_change_taxes()
        # Apply the customer tax rules even when there is no product selected because of the default taxes
        if not self.product:
            if self.sale and self.sale.party and self.sale.party.customer_tax_rule and self.type == 'line':
                taxes = []
                pattern = self._get_tax_rule_pattern()
                for tax in self.taxes:
                    tax_ids = self.sale.party.customer_tax_rule.apply(tax, pattern)
                    if tax_ids:
                        taxes.extend(tax_ids)
                if taxes:
                    self.taxes = taxes

    def get_sale_party(self, name):
        return self.sale.party.id if self.sale and self.sale.party else None

    @classmethod
    def search_sale_party(cls, name, clause):
        return [('sale.party',) + tuple(clause[1:])]

    def get_shipment_state(self, name):
        """Return the shipment state for the sale line"""
        state = ""
        moves = self.get_moves()
        if moves:
            state = "waiting"
            received_quantity = sum(move.quantity for move in moves if move.state == 'done')
            if received_quantity >= self.get_quantity():
                state = "received"
            else:
                state = "partially_received"
        return state

    def get_moves(self):
        return self.moves

    def get_quantity(self):
        return self.quantity

    def get_invoice_state(self, name):
        """Return the invoice state for the sale line"""
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        if self.sale:
            invoice_lines = InvoiceLine.search([('origin', '=', str(self))])
            if invoice_lines and all(line.invoice for line in invoice_lines):
                skip_ids = set(x.id for x in self.sale.invoices_ignored)
                skip_ids.update(x.id for x in self.sale.invoices_recreated)
                invoices = [i for i in self.sale.invoices if i.id not in skip_ids]
                if any(i.state == 'cancelled' for i in invoices):
                    return 'exception'
                elif all(i.state == 'posted' for i in invoices):
                    return 'posted'
                elif all(i.state == 'paid' for i in invoices):
                    return 'paid'
                else:
                    return 'waiting'
        return 'none'

    @fields.depends('product', 'description', '_parent_sale.party', 'unit_price')
    def on_change_product(self):
        # If there is a price defined, do not change it although the product
        # is changed
        context = Transaction().context
        keep_price = context.get('keep_price', False) or bool(self.unit_price)
        if keep_price:
            unit_price = self.unit_price
            super(SaleLine, self).on_change_product()
            self.unit_price = unit_price
            self.gross_unit_price = unit_price
            self.amount = self.on_change_with_amount()
        else:
            super(SaleLine, self).on_change_product()

    def update_prices(self):
        # if there is no product selected and it's getting executed from ModifyHeader wizard,
        # do not modify the prices
        if not Transaction().context.get('keep_price', False):
            super(SaleLine, self).update_prices()

    def get_sale_date(self, name):
        return self.sale.sale_date if self.sale else None

    @classmethod
    def search_sale_date(cls, name, clause):
        return [('sale.sale_date',) + tuple(clause[1:])]

    @fields.depends('type', 'taxes')
    def on_change_type(self):

        if self.type == 'line':
            self.taxes = self.default_taxes()
        else:
            self.taxes = []

    @fields.depends('product', 'sale', '_parent_sale.price_list')
    def on_change_with_price_list_line_position(self, name=None):
        if self.product and self.sale and self.sale.price_list:
            for line in self.sale.price_list.lines:
                if line.product and self.product and\
                        self.product.id == line.product.id:
                    return line.position

    def get_move(self, shipment_type):
        move = super(SaleLine, self).get_move(shipment_type)
        if move and shipment_type == 'out':
            move.package_type = self.package_type or (self.sale.package_type if self.sale else None)
        return move

    @property
    def invoiced_quantity(self):
        # Used for invoice valued relation report, to get the invoiced
        # quantity of the origin sale line of each line in the invoice
        pool = Pool()
        Uom = pool.get('product.uom')
        invoiced_quantity = 0
        for invoice_line in self.invoice_lines:
            if invoice_line.type != 'line':
                continue
            if invoice_line.invoice and invoice_line.invoice.state in ['posted', 'paid']:
                invoiced_quantity += Uom.compute_qty(invoice_line.unit,
                    invoice_line.quantity, self.unit)
        return invoiced_quantity


class SaleGenerateLinePositions(Wizard):
    'Sale Generate Positions'
    __name__ = 'sale.sale.generate_line_positions'

    start_state = 'generate_line_positions_start'
    generate_line_positions_start = StateTransition()

    @staticmethod
    def transition_generate_line_positions_start():
        pool = Pool()
        Sale = pool.get('sale.sale')
        Line = pool.get('sale.line')
        for sale in Sale.browse(Transaction().context['active_ids']):
            # only returns de lines which position has been modified
            modified_lines = generate_line_positions(sale.lines)
            Line.save(modified_lines)
        return 'end'


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'

    sale_quotation_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Sale Quotation Sequence", required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('sale', 'sequence_type_sale')),
        ]))
    default_tax = fields.MultiValue(fields.Many2One(
        'account.tax', "Default Tax"))
    default_in_force_period = fields.TimeDelta('Force Period')
    sales_department_email = fields.Many2One('party.contact_mechanism',
        'Email from the Sales Department')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'sale_quotation_sequence':
            return pool.get('sale.configuration.sequence')
        elif field == 'default_tax':
            return pool.get('sale.configuration.tax')
        return super(Configuration, cls).multivalue_model(field)


class ConfigurationSequence(metaclass=PoolMeta):
    "Sale Configuration Sequence"
    __name__ = 'sale.configuration.sequence'

    sale_quotation_sequence = fields.Many2One(
        'ir.sequence', "Sale Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=', Id('sale', 'sequence_type_sale')),
        ],
        depends=['company'])


class ConfigurationTax(ModelSQL, CompanyValueMixin):
    "Sale Configuration Tax"
    __name__ = 'sale.configuration.tax'

    default_tax = fields.Many2One('account.tax', 'Default Tax', required=True)


class SaleImportLineStart(ModelView):
    'Sale Import Line Start'
    __name__ = 'sale.import_line.start'

    lines = fields.One2Many(
        'sale.line', None, "Lines",
        domain=[('sale.not_from_presupuestario', '=', True), ('sale.origin', '=', None),
                If((Eval('sale')),
                   ('sale', '=', Eval('sale')),
                   ())],
        depends=['sale'])
    sale = fields.Many2One('sale.sale', 'Sale')


def get_biggest_sequence(sale):
    # get the biggest sequence in sale lines to set to the new lines
    # one bigger to place them to the last position
    sequences = [line.sequence for line in sale.lines if line.sequence]
    return sequences[-1] + 1 if sequences else 1


class SaleImportLine(Wizard):
    'Sale Import Line'
    __name__ = 'sale.import_line'
    start = StateTransition()
    ask_sale_line = StateView('sale.import_line.start',
                      'electrans_sale.sale_import_line_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Import', 'import_lines', 'tryton-ok', default=True),
                      ])
    import_lines = StateTransition()

    def transition_start(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        if sale.not_from_presupuestario and not sale.origin:
            if sale.orders:
                raise UserError(gettext('electrans_sale.has_orders'))
            if sale.state != 'draft':
                raise UserError(gettext('electrans_sale.not_sale_draft'))
            return 'ask_sale_line'
        else:
            raise UserError(gettext('electrans_sale.not_sale_quotation'))

    def transition_import_lines(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        sale = Sale(Transaction().context['active_id'])
        sequence = get_biggest_sequence(sale)
        # copy the lines we want to import from wizard to lines list
        lines = SaleLine.copy(self.ask_sale_line.lines, {'sequence': sequence})
        # add imported lines to existing lines of the sale
        Sale.write([sale], {'lines': [('add', lines)]})
        for l in lines:
            # Only fill taxes if type line, if is not type line it will never have tax so do nothing with the copy
            if l.type == 'line':
                # compute_taxes() define the tax of the imported lines when exists
                # product, from customer_taxes_used of product and from
                # customer_tax_rule of the customer of the sale
                if l.product:
                    l.taxes = l.compute_taxes(sale.party)
                else:
                    # else, we get the tax from default sales configuration
                    l.taxes = l.default_taxes()
        SaleLine.save(lines)
        return 'end'


class SaleDuplicateLine(Wizard):
    'Sale Import Line'
    __name__ = 'sale.duplicate_line'
    start = StateTransition()
    ask_sale_line = StateView('sale.import_line.start',
                      'electrans_sale.sale_import_line_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Import', 'import_lines', 'tryton-ok', default=True),
                      ])
    import_lines = StateTransition()

    def transition_start(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        if sale.not_from_presupuestario and not sale.origin:
            if sale.orders:
                raise UserError(gettext('electrans_sale.has_orders'))
            return 'ask_sale_line'
        else:
            raise UserError(gettext('electrans_sale.not_sale_quotation'))

    def default_ask_sale_line(self, fields):
        Sale = Pool().get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        return {'sale': sale.id}

    def transition_import_lines(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        sale = Sale(Transaction().context['active_id'])
        sequence = get_biggest_sequence(sale)
        lines = SaleLine.copy(self.ask_sale_line.lines, {'sequence': sequence})
        Sale.write([sale], {
            'lines': [('add', lines)]})
        return 'end'


class ProductCustomer(metaclass=PoolMeta):
    __name__ = 'sale.product_customer'

    @classmethod
    def __setup__(cls):
        super(ProductCustomer, cls).__setup__()
        cls.name.translate = False


class ModifyHeader(metaclass=PoolMeta):
    __name__ = 'sale.modify_header'

    def transition_modify(self):
        # APP-3471: The next line is needed to keep the particular terms after complete the wizard
        self.start.particular_terms = self.record.particular_terms
        with Transaction().set_context(keep_price=True):
            return super(ModifyHeader, self).transition_modify()


class SaleLinePercentageStart(ModelView):
    'Sale Lines Discount Start'
    __name__ = 'sale.line.percentage.start'

    lines = fields.One2Many('sale.line', None, "Lines",
        domain=[('sale', '=', Eval('context', {}).get('active_id', -1)),
                ('type', '=', 'line'),
                ])
    percentage = fields.Numeric("Percentage", digits=(1, 3), required=True)


class SaleLinesDiscount(Wizard):
    'Sale Lines Discount'
    __name__ = 'sale.line_discount'

    start = StateTransition()
    ask_sale_line = StateView('sale.line.percentage.start',
                      'electrans_sale.sale_line_percentage_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Apply', 'apply_discount_lines', 'tryton-ok', default=True),
                      ])
    apply_discount_lines = StateTransition()

    def transition_start(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        if sale.not_from_presupuestario and not sale.origin:
            if sale.state != 'draft':
                raise UserError(gettext('electrans_sale.not_sale_draft'))
            return 'ask_sale_line'
        else:
            raise UserError(gettext('electrans_sale.not_sale_quotation'))

    def transition_apply_discount_lines(self):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        for line in self.ask_sale_line.lines:
            line.discount = self.ask_sale_line.percentage
            line.on_change_discount()
        SaleLine.save(self.ask_sale_line.lines)
        return 'end'


class SaleLinesAmount(Wizard):
    'Sale Lines Amount'
    __name__ = 'sale.line_amount'

    start = StateTransition()
    ask_sale_line = StateView('sale.line.percentage.start',
                      'electrans_sale.sale_line_percentage_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Apply', 'apply', 'tryton-ok', default=True),
                      ])
    apply = StateTransition()

    def transition_start(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        if sale.not_from_presupuestario and not sale.origin:
            if sale.state != 'draft':
                raise UserError(gettext('electrans_sale.not_sale_draft'))
            return 'ask_sale_line'
        else:
            raise UserError(gettext('electrans_sale.not_sale_quotation'))

    def transition_apply(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        sale = Sale(Transaction().context['active_id'])
        for line in self.ask_sale_line.lines:
            line.gross_unit_price = sale.currency.round(
                line.gross_unit_price * (Decimal(1.0) + self.ask_sale_line.percentage))
            line.on_change_gross_unit_price()
        SaleLine.save(self.ask_sale_line.lines)

        return 'end'


class SaleLineMoveStart(ModelView):
    'Sale Line Move Start'
    __name__ = 'sale.line_move.start'

    lines_to_move = fields.One2Many(
        'sale.line', None, "Lines to move",
        required=True,
        domain=[('sale', '=', Eval('context', {}).get('active_id', -1))])
    position = fields.Selection([
        ('before', 'Before'),
        ('after', 'After')],
        "Position", required=True)
    line_position = fields.Many2One(
        'sale.line', "Line Position",
        domain=[('sale', '=', Eval('context', {}).get('active_id', -1))])


class SaleLineMove(Wizard):
    'Sale Line Moves'
    __name__ = 'sale.line_move'

    start = StateTransition()
    ask_sale_line = StateView('sale.line_move.start',
                      'electrans_sale.sale_line_move_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Move', 'move', 'tryton-ok', default=True),
                      ])
    move = StateTransition()

    def transition_start(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        if sale.state != 'draft':
            raise UserError(gettext('electrans_sale.not_sale_draft'))
        return 'ask_sale_line'

    def transition_move(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        move_lines(sale.lines, self.ask_sale_line.lines_to_move, self.ask_sale_line.position,
                   self.ask_sale_line.line_position)
        return 'end'


class CreateOrderStart(ModelView):
    "Create Order Start"
    __name__ = 'sale.create_order.start'

    reference = fields.Char(
        "Reference")
    name = fields.Char(
        "Name", readonly=True)
    attachment = fields.Binary(
        "Attachment", filename='name')


class CreateOrder(Wizard):
    "Create Order"
    __name__ = 'sale.create_order'

    start = StateTransition()
    ask_sale = StateView(
        'sale.create_order.start',
        'electrans_sale.sale_create_order_start', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_', 'tryton-go-next', default=True)
        ])
    create_ = StateAction('electrans_sale.act_sale_order')

    def transition_start(self):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        Sale = pool.get('sale.sale')
        sale = Sale(self.record.id)
        if sale.is_order:
            raise UserError(gettext('electrans_sale.not_allow_to_create_an_order'))
        if sale.state != 'quotation':
            raise UserError(gettext('electrans_sale.not_quotation'))
        if not sale.in_force:
            raise UserError(gettext('electrans_sale.not_in_force'))
        if not sale.party.allowed_for_sales:
            raise UserError(gettext('electrans_sale.not_allowed_for_sales'))
        if sale.party.customer_payment_type and sale.payment_type and (
                sale.party.customer_payment_type != sale.payment_type):
            key = '%s_not_same_customer_payment_type%s' % (sale.id, sale.payment_type.id)
            if Warning.check(key):
                raise UserWarning(key, gettext('electrans_sale.not_same_customer_payment_type'))
        return 'ask_sale'

    def copy_sale(self):
        # This function is created to let other modules override it easily
        Sale = Pool().get('sale.sale')
        sale, = Sale.copy([self.record], {
            'new_order_from_wizard': True,
            'origin': str(self.record),
            'reference': self.ask_sale.reference,
            'lines': None
        })
        return sale

    def do_create_(self, action):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        Attachment = pool.get('ir.attachment')
        quotation = self.record
        sale = self.copy_sale()
        # define the employee after creation because if not, the employee who
        # execute the wizard becomes the seller
        sale.employee = quotation.employee.id if quotation.employee else None
        sale.save()
        # link the new sale lines with the quotation ones
        for line in quotation.lines:
            # copy function doesn't copy the dates so it must bespecify
            SaleLine.copy([line], {
                'quotation_line': line.id,
                'sale': sale.id,
                'manual_delivery_date': line.manual_delivery_date})
        if self.ask_sale.attachment:
            Attachment.create([{
                'name': self.ask_sale.name,
                'type': 'data',
                'data': self.ask_sale.attachment,
                'resource': str(sale), 'main': True}])
        data = {'res_id': [sale.id]}
        action['views'].reverse()
        return action, data


class LineTax(metaclass=PoolMeta):
    __name__ = 'sale.line-account.tax'
    _history = True

class SaleTaxChange(Wizard):
    'Sale Tax Change'
    __name__ = 'sale.sale.sale_tax_change'

    start = StateView('sale.sale.sale_tax_change.start',
                      'electrans_sale.sale_tax_change_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Confirm', 'tax_change_start', 'tryton-ok', default=True),
                      ])
    tax_change_start = StateTransition()

    def default_start(self, fields):
        defaults = {}
        if self.record.state != 'draft':
            raise UserError(gettext('electrans_sale.not_draft_in_change_tax'))
        defaults['company'] = self.record.company.id
        return defaults

    def transition_tax_change_start(self):
        """
        This function changes the taxes of all the lines of the sale at the same time
        """
        pool = Pool()
        SaleLine = pool.get('sale.line')
        for line in self.record.lines:
            if line.type == 'line':
                line.taxes = (self.start.tax,)
        SaleLine.save(self.record.lines)
        self.record.on_change_lines()
        self.record.save()
        return 'end'


class SaleTaxChangeStart(ModelView):
    'Sale Tax Change Start'
    __name__ = 'sale.sale.sale_tax_change.start'

    sale = fields.Many2One('sale.sale', 'Sale')
    company = fields.Many2One('company.company', 'Company')
    tax = fields.Many2One('account.tax', 'Tax', domain=[('parent', '=', None), ['OR',
                ('group', '=', None),
                ('group.kind', 'in', ['sale', 'both'])],
                ('company', '=', Eval('company'))
            ], depends=['company'])


class HandleInvoiceException(metaclass=PoolMeta):
    __name__ = 'sale.handle.invoice.exception'

    def default_ask(self, fields):
        default = super(HandleInvoiceException, self).default_ask(fields)
        default['recreate_invoices'] = []
        return default


class HandleShipmentException(metaclass=PoolMeta):
    __name__ = 'sale.handle.shipment.exception'

    def default_ask(self, fields):
        default = super(HandleShipmentException, self).default_ask(fields)
        default['recreate_moves'] = []
        return default
