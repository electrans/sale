# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import sale
from . import template
from . import stock

def register():
    Pool.register(
        party.Party,
        sale.Configuration,
        sale.ConfigurationSequence,
        sale.ConfigurationTax,
        sale.CreateOrderStart,
        sale.LineTax,
        sale.ProductCustomer,
        sale.Sale,
        sale.SaleCategory,
        sale.SaleImportLineStart,
        sale.SaleLine,
        sale.SaleLineMoveStart,
        sale.SaleLinePercentageStart,
        sale.SaleTaxChangeStart,
        stock.Package,
        stock.StockMove,
        stock.ShipmentOut,
        template.Template,
        module='electrans_sale', type_='model')
    Pool.register(
        sale.SaleGenerateLinePositions,
        sale.SaleImportLine,
        sale.SaleDuplicateLine,
        sale.ModifyHeader,
        sale.SaleLinesDiscount,
        sale.SaleLinesAmount,
        sale.SaleLineMove,
        sale.CreateOrder,
        sale.SaleTaxChange,
        sale.HandleInvoiceException,
        sale.HandleShipmentException,
        module='electrans_sale', type_='wizard')
