# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
try:
    from trytond.modules.electrans_sale.tests.test_electrans_sale import suite
except ImportError:
    from .test_electrans_sale import suite

__all__ = ['suite']
