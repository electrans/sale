#!/usr/bin/env python
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.modules.company.tests import create_company, set_company, create_employee
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.exceptions import UserError, UserWarning
from trytond.model.exceptions import DomainValidationError
from decimal import Decimal
from trytond.transaction import Transaction
from trytond.modules.electrans_tools.tests.test_tools import (
    create_fiscalyear_and_chart, get_accounts, create_product, create_account_category, create_sequence)
import datetime


class TestSaleCase(ModuleTestCase):
    'Test module'
    module = 'electrans_sale'

    # Creates a sale with default values
    def create_sale(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Party = pool.get('party.party')
        Location = pool.get('stock.location')
        Config = pool.get('sale.configuration')
        config = Config(1)
        sequence = create_sequence('Offer', 'Sale')
        config.sale_quotation_sequence = sequence
        config.save()
        company = create_company()
        with set_company(company):
            party, = Party.create([{
                'name': 'Test Sale Party',
                'addresses': [('create', [{}])],
                'allowed_for_sales': True,
            }])
            party.addresses[0].invoice = True
            party.addresses[0].save()
            warehouse, = Location.search([('code', '=', 'WH')])
            sale, = Sale.create([{
                'party': party.id,
                'warehouse': warehouse.id,
                'shipment_address': party.addresses[0].id,
                'invoice_address': party.addresses[0].id,
                'address': party.addresses[0].id,
            }])

            return sale

    @with_transaction()
    def test_move_sale_lines(self):
        "Test Move Sale Lines"
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Location = pool.get('stock.location')
        company = create_company()
        with set_company(company):
            # Get warehouses and production location
            warehouse, = Location.search([('code', '=', 'WH')])
            # Get party
            employee = create_employee(company, 'Party1')
            employee.party.allowed_for_sales = True
            employee.party.save()
            self.assertIsNotNone(employee.party.addresses)
            employee.party.addresses[0].invoice = True
            employee.party.addresses[0].save()
            # Create sale and lines
            sale = Sale()
            sale.party = employee.party
            sale.warehouse = warehouse
            sale.in_force_period = datetime.timedelta(days=120)
            sale.shipment_address = employee.party.addresses[0]
            sale.invoice_address = employee.party.addresses[0]
            sale.address = employee.party.addresses[0]
            sale.not_from_presupuestario = True
            sale.save()
            for i in range(0, 5):
                sale_line = SaleLine()
                sale_line.sale = sale
                sale_line.quantity = 1
                sale_line.gross_unit_price = Decimal('1.00')
                sale_line.unit_price = Decimal('1.00')
                sale_line.position = str(i+1) + '.'
                sale_line.save()
            self.assertEqual(sale.lines[0].position, '1.')
            self.assertEqual(sale.lines[1].position, '2.')
            self.assertEqual(sale.lines[2].position, '3.')
            self.assertEqual(sale.lines[3].position, '4.')
            self.assertEqual(sale.lines[4].position, '5.')
            # Execute Move Lines Wizard
            WizardMoveLines = pool.get(
                'sale.line_move',
                type='wizard')
            # If sale is in draft state and can move the lines after one defined line
            session_id, _, _ = WizardMoveLines.create()
            wizard_move_lines = WizardMoveLines(session_id)
            wizard_move_lines.records = [sale]
            with Transaction().set_context(active_id=sale):
                wizard_move_lines.transition_start()
                wizard_move_lines.ask_sale_line.lines_to_move = sale.lines[0:4]
                wizard_move_lines.ask_sale_line.position = 'after'
                wizard_move_lines.ask_sale_line.line_position = sale.lines[4]
                wizard_move_lines.transition_move()
            self.assertEqual(sale.lines[0].position, '5.')
            self.assertEqual(sale.lines[1].position, '1.')
            self.assertEqual(sale.lines[2].position, '2.')
            self.assertEqual(sale.lines[3].position, '3.')
            self.assertEqual(sale.lines[4].position, '4.')
            # If sale is in draft state and can move the lines before one defined line
            session_id, _, _ = WizardMoveLines.create()
            wizard_move_lines = WizardMoveLines(session_id)
            wizard_move_lines.records = [sale]
            with Transaction().set_context(active_id=sale.id):
                wizard_move_lines.transition_start()
                wizard_move_lines.ask_sale_line.lines_to_move = sale.lines[2:5]
                wizard_move_lines.ask_sale_line.position = 'before'
                wizard_move_lines.ask_sale_line.line_position = sale.lines[1]
                wizard_move_lines.transition_move()
            self.assertEqual(sale.lines[0].position, '5.')
            self.assertEqual(sale.lines[1].position, '2.')
            self.assertEqual(sale.lines[2].position, '3.')
            self.assertEqual(sale.lines[3].position, '4.')
            self.assertEqual(sale.lines[4].position, '1.')
            # If sale is not in draft state
            Sale.confirm([sale])
            self.assertEqual(sale.state, 'confirmed')
            session_id, _, _ = WizardMoveLines.create()
            wizard_move_lines = WizardMoveLines(session_id)
            wizard_move_lines.records = [sale]
            with Transaction().set_context(active_id=sale):
                with self.assertRaises(UserError):
                    wizard_move_lines.transition_start()

    @with_transaction()
    def test_warning_sale_payment_type_different_of_customer(self):
        "Test Warning Payment Type Different of Customer"
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Location = pool.get('stock.location')
        SaleCategory = pool.get('sale.category')
        Country = pool.get('country.country')
        PaymentType = pool.get('account.payment.type')
        sale_country = Country(name='Spain')
        sale_category = SaleCategory(name='SaleCategory')
        Config = pool.get('sale.configuration')
        config = Config(1)
        sequence = create_sequence('Offer', 'Sale')
        config.sale_quotation_sequence = sequence
        config.save()
        company = create_company()
        with set_company(company):
            # Create payment types
            payment_type = PaymentType(name='Bank Transfer')
            payment_type_changed = PaymentType(name='Will pay')
            # Get warehouses and production location
            warehouse, = Location.search([('code', '=', 'WH')])
            # Get party
            employee = create_employee(company, 'Party1')
            employee.party.allowed_for_sales = True
            employee.party.customer_payment_type = payment_type
            employee.party.save()
            # Create sale and lines
            sale = Sale()
            sale.category = sale_category
            sale.country = sale_country
            sale.party = employee.party
            sale.warehouse = warehouse
            sale.in_force_period = datetime.timedelta(days=120)
            sale.not_from_presupuestario = True
            # Change the party payment type
            sale.payment_type = payment_type_changed
            sale.save()
            sale_line = SaleLine()
            sale_line.sale = sale
            sale_line.quantity = 1
            sale_line.gross_unit_price = Decimal('1.00')
            sale_line.unit_price = Decimal('1.00')
            sale_line.save()
            Sale.quote([sale])
            self.assertEqual(sale.state, 'quotation')
            # Execute Create Order Wizard
            WizarCreateOrder = pool.get('sale.create_order', type='wizard')
            session_id, _, _ = WizarCreateOrder.create()
            wizard_create_order = WizarCreateOrder(session_id)
            wizard_create_order.record = sale
            self.assertNotEqual(sale.party.customer_payment_type, sale.payment_type)
            with self.assertRaises(UserWarning):
                wizard_create_order.transition_start()

    @with_transaction()
    def test_invoiced_amount(self):
        "Test Invoiced Amount"
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Invoice = pool.get('account.invoice')
        Location = pool.get('stock.location')
        SaleCategory = pool.get('sale.category')
        Country = pool.get('country.country')
        sale_country = Country(name='Spain')
        sale_category = SaleCategory(name='SaleCategory')
        Config = pool.get('sale.configuration')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        config = Config(1)
        sequence = create_sequence('Offer', 'Sale')
        config.sale_quotation_sequence = sequence
        config.save()
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            revenue = accounts.get('revenue')
            expense = accounts.get('expense')
            tax, = Tax.search([], limit=1)
            account_category_goods = create_account_category(tax, expense, revenue)
            # Get warehouses and production location
            warehouse, = Location.search([('code', '=', 'WH')])
            # Get party
            employee = create_employee(company, 'Party1')
            employee.party.allowed_for_sales = True
            employee.party.save()
            self.assertIsNotNone(employee.party.addresses)
            employee.party.addresses[0].invoice = True
            employee.party.addresses[0].save()
            # Create goods product
            template, product = create_product('Product 1', account_category_goods)
            template.salable = True
            template.sale_uom = unit.id
            template.save()
            # Create sale and lines
            sale = Sale()
            sale.category = sale_category
            sale.country = sale_country
            sale.party = employee.party
            sale.warehouse = warehouse
            sale.invoice_method = 'order'
            sale.in_force_period = datetime.timedelta(days=120)
            sale.shipment_address = employee.party.addresses[0]
            sale.invoice_address = employee.party.addresses[0]
            sale.address = employee.party.addresses[0]
            sale.not_from_presupuestario = True
            sale.save()
            sale_line = SaleLine()
            sale_line.sale = sale
            sale_line.quantity = 1
            sale_line.product = product
            sale_line.unit = unit
            sale_line.gross_unit_price = Decimal('10.00')
            sale_line.unit_price = Decimal('10.00')
            sale_line.position = '1.'
            sale_line.save()
            self.assertEqual(len(sale.lines), 1)
            Sale.quote([sale])
            self.assertEqual(sale.state, 'quotation')
            # Execute Create Order Wizard
            WizarCreateOrder = pool.get('sale.create_order', type='wizard')
            session_id, _, _ = WizarCreateOrder.create()
            wizard_create_order = WizarCreateOrder(session_id)
            wizard_create_order.record = sale
            with Transaction().set_context(active_id=sale.id):
                wizard_create_order.ask_sale.reference = None
                wizard_create_order.ask_sale.attachment = None
                wizard_create_order.transition_start()
                self.assertEqual(len(sale.orders), 0)
                wizard_create_order._execute('create_')
                self.assertEqual(len(sale.orders), 1)
            # Process order
            sale_order = sale.orders[0]
            Sale.confirm(sale.orders)
            self.assertEqual(sale_order.state, 'confirmed')
            Sale.process(sale.orders)
            self.assertEqual(sale_order.state, 'processing')
            # Check Invoice has been created, as the invoice_method is based on order when processing it
            self.assertEqual(len(sale_order.invoices), 1)
            self.assertEqual(len(sale_order.invoices[0].lines), 1)
            self.assertEqual(sale_order.invoices[0].untaxed_amount, Decimal('10.0'))
            self.assertEqual(sale_order.invoices[0].state, 'draft')
            # Check that when the invoice is posted the invoiced_amount is updated
            self.assertEqual(sale_line.invoiced_amount, Decimal('0.0'))
            Invoice.post(sale_order.invoices)
            self.assertEqual(sale_order.invoices[0].state, 'posted')
            self.assertEqual(sale_order.lines[0].invoiced_amount, Decimal('10.0'))
            self.assertEqual(sale_order.lines[0].amount, Decimal('10.0'))

    @with_transaction()
    def test_package_shipment_out(self):
        "Test Package Shipment Out"
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Lot = pool.get('stock.lot')
        StockMove = pool.get('stock.move')
        ShipmentIn = pool.get('stock.shipment.in')
        Carrier = pool.get('carrier')
        Location = pool.get('stock.location')
        PurchaseLine = pool.get('purchase.line')
        Purchase = pool.get('purchase.purchase')
        ShipmentOut = pool.get('stock.shipment.out')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        PackageType = pool.get('stock.package.type')
        SaleCategory = pool.get('sale.category')
        Country = pool.get('country.country')
        sale_country = Country(name='Spain')
        sale_category = SaleCategory(name='SaleCategory')
        Config = pool.get('sale.configuration')
        config = Config(1)
        sequence = create_sequence('Offer', 'Sale')
        config.sale_quotation_sequence = sequence
        config.save()
        company = create_company()
        with set_company(company):
            # Create all necessary locations
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            supplier_loc, = Location.search([('code', '=', 'SUP')])
            warehouse.storage_location = storage_location
            warehouse.save()
            # Get party
            employee = create_employee(company, 'Party1')
            employee.party.allowed_for_sales = True
            employee.party.save()
            self.assertIsNotNone(employee.party.addresses)
            employee.party.addresses[0].invoice = True
            employee.party.addresses[0].save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            revenue = accounts.get('revenue')
            expense = accounts.get('expense')
            tax, = Tax.search([], limit=1)
            account_category_goods = create_account_category(tax, expense, revenue)
            account_category = create_account_category(tax, expense, revenue, "Servicios")
            # Create goods product
            template, product = create_product('Product 1', account_category_goods)
            template.salable = True
            template.sale_uom = unit.id
            template.save()
            # Create Supplier Shipment to have stock
            shipment_in = ShipmentIn()
            shipment_in.planned_date = datetime.date.today()
            shipment_in.supplier = employee.party
            shipment_in.warehouse = warehouse
            shipment_in.save()
            # Create lot
            lot = Lot()
            lot.number = '1'
            lot.product = product
            lot.save()
            # Create a purchase line origin
            purchase = Purchase()
            purchase.party = employee.party
            purchase.warehouse = warehouse
            purchase.save()
            purchase_line = PurchaseLine()
            purchase_line.quantity = 1
            purchase_line.unit_price = Decimal('1.00')
            purchase_line.purchase = purchase
            purchase_line.unit = unit
            purchase_line.save()
            # Create Move
            move = StockMove()
            move.product = product
            move.shipment = shipment_in
            move.uom = unit
            move.quantity = 1
            move.unit_price = Decimal('1.00')
            move.cost_price = Decimal('1.00')
            move.from_location = supplier_loc
            move.to_location = warehouse.input_location
            move.origin = purchase_line
            move.lot = lot
            move.save()
            self.assertEqual(shipment_in.state, 'draft')
            self.assertEqual(len(shipment_in.incoming_moves), 1)
            ShipmentIn.receive([shipment_in])
            self.assertEqual(shipment_in.incoming_moves[0].state, 'done')
            self.assertEqual(len(shipment_in.inventory_moves), 1)
            self.assertEqual(shipment_in.inventory_moves[0].state, 'draft')
            self.assertEqual(shipment_in.state, 'received')
            ShipmentIn.done([shipment_in])
            self.assertEqual(shipment_in.state, 'done')
            self.assertEqual(shipment_in.inventory_moves[0].state, 'done')
            # Create package type
            package_type = PackageType(name='Box')
            package_type.save()
            # Create sale and lines
            sale = Sale()
            sale.package_type = package_type
            sale.category = sale_category
            sale.country = sale_country
            sale.party = employee.party
            sale.warehouse = warehouse
            sale.invoice_method = 'order'
            sale.in_force_period = datetime.timedelta(days=120)
            sale.shipment_address = employee.party.addresses[0]
            sale.invoice_address = employee.party.addresses[0]
            sale.address = employee.party.addresses[0]
            sale.not_from_presupuestario = True
            sale.save()
            sale_line = SaleLine()
            sale_line.sale = sale
            sale_line.quantity = 1
            sale_line.product = product
            sale_line.package_type = package_type
            sale_line.unit = unit
            sale_line.gross_unit_price = Decimal('1.00')
            sale_line.unit_price = Decimal('1.00')
            sale_line.save()
            self.assertEqual(len(sale.lines), 1)
            Sale.quote([sale])
            self.assertEqual(sale.state, 'quotation')
            # Execute Create Order Wizard
            WizarCreateOrder = pool.get('sale.create_order', type='wizard')
            session_id, _, _ = WizarCreateOrder.create()
            wizard_create_order = WizarCreateOrder(session_id)
            wizard_create_order.record = sale
            with Transaction().set_context(active_id=sale.id):
                wizard_create_order.ask_sale.reference = None
                wizard_create_order.ask_sale.attachment = None
                wizard_create_order.transition_start()
                self.assertEqual(len(sale.orders), 0)
                wizard_create_order._execute('create_')
                self.assertEqual(len(sale.orders), 1)
                self.assertEqual(len(sale.orders[0].lines), 1)
            sale_order = sale.orders[0]
            sale_order_line = sale.orders[0].lines[0]
            # Process order
            self.assertIsNotNone(sale.package_type)
            self.assertIsNotNone(sale.lines[0].package_type)
            self.assertEqual(sale_order_line.package_type, sale.lines[0].package_type)
            self.assertEqual(sale_order.package_type, sale.package_type)
            Sale.confirm(sale.orders)
            self.assertEqual(len(sale_order.shipments), 0)
            self.assertEqual(sale_order.state, 'confirmed')
            Sale.process(sale.orders)
            self.assertEqual(sale_order.state, 'processing')
            self.assertEqual(len(sale_order.shipments), 1)
            shipment = sale_order.shipments[0]
            self.assertEqual(len(shipment.outgoing_moves), 1)
            self.assertEqual(shipment.outgoing_moves[0].package_type, sale_order_line.package_type)
            # Create service product for carrier
            service_template, service = create_product('Service', account_category)
            service_template.purchasable = True
            service_template.save()
            carrier, = Carrier.create([{
                'party': employee.party.id,
                'carrier_product': service.id,
                'carrier_cost_method': 'product'
            }])
            shipment.carrier = carrier
            shipment.save()
            ShipmentOut.wait([shipment])
            self.assertEqual(shipment.state, 'waiting')
            ShipmentOut.assign_try([shipment])
            self.assertEqual(shipment.state, 'assigned')
            ShipmentOut.pick([shipment])
            self.assertEqual(shipment.state, 'picked')
            ShipmentOut.pack([shipment])
            self.assertEqual(shipment.state, 'packed')

    @with_transaction()
    def test_sale_invoice_address_domain(self):
        ' Test sale invoice address domain '
        pool = Pool()
        Sale = pool.get('sale.sale')
        company = create_company()
        with set_company(company):
            sale = self.create_sale()
            # Assert sale is in draft state
            self.assertEqual(sale.state, 'draft')
            sale.invoice_address.invoice = False
            sale.invoice_address.save()
            # Assert domain error is being raised when trying to confirm a sale
            # without the invoice check marked in the current invoice address
            self.assertRaises(DomainValidationError, Sale.confirm, [sale])
            sale.invoice_address.invoice = True
            sale.invoice_address.save()
            Sale.confirm([sale])
            # Assert sale is in confirmed state
            self.assertEqual(sale.state, 'confirmed')
            sale.invoice_address.invoice = False
            sale.invoice_address.save()
            # This will fail if the domain is not working correctly, it checks
            # the domain is not raising errors when switching back to 'draft'
            # I have to check it like this as there's nothing like
            # 'self.assertNotRaises'
            Sale.draft([sale])
            self.assertEqual(sale.state, 'draft')
            # Assert domain error trying to quote
            self.assertRaises(DomainValidationError, Sale.quote, [sale])
            sale.invoice_address.invoice = True
            sale.invoice_address.save()
            Sale.quote([sale])
            # Assert sale is in quotation state
            self.assertEqual(sale.state, 'quotation')


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestSaleCase))
    return suite


