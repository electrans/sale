# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.i18n import gettext


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @classmethod
    def write(cls, *args):
        pool = Pool()
        Email = pool.get('ir.email')
        Product = pool.get('product.product')
        SaleConfiguration = pool.get('sale.configuration')
        SaleLine = pool.get('sale.line')
        sale_config = SaleConfiguration(1)
        super(Template, cls).write(*args)
        actions = iter(args)
        for templates, values in zip(actions, actions):
            # Check if products are no longer salable
            if 'salable' in values and not values['salable']:
                for template in templates:
                    # Check if there are products with sale quotation lines
                    products = Product.search([('template', '=', template.id)])
                    sale_lines = SaleLine.search([
                        ('product', 'in', [
                            product.id for product in products]),
                        ('sale.state', 'in', ['draft', 'quotation']),
                        ('sale.not_from_presupuestario', '=', True),
                        ('sale.is_order', '=', False),
                        ('sale.orders', '=', None),
                    ])
                    if sale_lines:
                        # Prepare the email body with products and sales
                        body = gettext(
                            'electrans_sale.product_not_salable_in_sales',
                            products=', '.join(set(
                                product.rec_name for product in products)),
                            sales=', '.join(
                                set(line.sale.number or line.sale.description
                                    or line.sale.rec_name
                                    for line in sale_lines))
                        )
                        # Send the email to the sales_department_email
                        Email.send(
                            to=sale_config.sales_department_email.email,
                            subject=gettext(
                                'electrans_sale.product_no_longer_salable'),
                            body=body,
                            record=[cls.__name__, template.id],
                            files=[])
